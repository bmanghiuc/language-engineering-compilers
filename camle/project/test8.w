{COMS22201 test1: program to test boolean operators <, >=, >}
{Already tested: write statements, real numbers and constants.}

if 1 < 2
  then write(1)
  else skip;
if 2 < 1
  then write(1)
  else skip;
writeln;
if 2 > 3
  then write(2)
  else skip;
if 3.5 > 2.7
  then write(2)
  else skip;
writeln;
if 7 >= 7
  then write(7.5)
  else skip;
if 6.9 >= 7.1
  then skip
  else write(8);
writeln
