// $ANTLR 3.5.2 Lex.g 2015-12-16 17:01:47

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class Lex extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ASSIGN=5;
	public static final int CLOSEPAREN=6;
	public static final int COMMENT=7;
	public static final int DIV=8;
	public static final int DO=9;
	public static final int ELSE=10;
	public static final int EQUAL=11;
	public static final int FALSE=12;
	public static final int GREATER=13;
	public static final int GREATEREQUAL=14;
	public static final int IF=15;
	public static final int INTNUM=16;
	public static final int LESS=17;
	public static final int LESSEQUAL=18;
	public static final int MINUS=19;
	public static final int MULTIPLY=20;
	public static final int NON=21;
	public static final int OPENPAREN=22;
	public static final int OR=23;
	public static final int PLUS=24;
	public static final int READ=25;
	public static final int REALNUM=26;
	public static final int SEMICOLON=27;
	public static final int SKIP=28;
	public static final int STRING=29;
	public static final int THEN=30;
	public static final int TRUE=31;
	public static final int VARIABLE=32;
	public static final int WHILE=33;
	public static final int WRITE=34;
	public static final int WRITELN=35;
	public static final int WS=36;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public Lex() {} 
	public Lex(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public Lex(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "Lex.g"; }

	// $ANTLR start "DO"
	public final void mDO() throws RecognitionException {
		try {
			int _type = DO;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:8:12: ( 'do' )
			// Lex.g:8:14: 'do'
			{
			match("do"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DO"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:9:12: ( 'else' )
			// Lex.g:9:14: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:10:12: ( 'false' )
			// Lex.g:10:14: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:11:12: ( 'if' )
			// Lex.g:11:14: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "READ"
	public final void mREAD() throws RecognitionException {
		try {
			int _type = READ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:12:12: ( 'read' )
			// Lex.g:12:14: 'read'
			{
			match("read"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "READ"

	// $ANTLR start "SKIP"
	public final void mSKIP() throws RecognitionException {
		try {
			int _type = SKIP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:13:12: ( 'skip' )
			// Lex.g:13:14: 'skip'
			{
			match("skip"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SKIP"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:14:12: ( 'then' )
			// Lex.g:14:14: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:15:12: ( 'true' )
			// Lex.g:15:14: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "WHILE"
	public final void mWHILE() throws RecognitionException {
		try {
			int _type = WHILE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:16:12: ( 'while' )
			// Lex.g:16:14: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHILE"

	// $ANTLR start "WRITE"
	public final void mWRITE() throws RecognitionException {
		try {
			int _type = WRITE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:17:12: ( 'write' )
			// Lex.g:17:14: 'write'
			{
			match("write"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WRITE"

	// $ANTLR start "WRITELN"
	public final void mWRITELN() throws RecognitionException {
		try {
			int _type = WRITELN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:18:12: ( 'writeln' )
			// Lex.g:18:14: 'writeln'
			{
			match("writeln"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WRITELN"

	// $ANTLR start "SEMICOLON"
	public final void mSEMICOLON() throws RecognitionException {
		try {
			int _type = SEMICOLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:23:14: ( ';' )
			// Lex.g:23:16: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMICOLON"

	// $ANTLR start "OPENPAREN"
	public final void mOPENPAREN() throws RecognitionException {
		try {
			int _type = OPENPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:24:14: ( '(' )
			// Lex.g:24:16: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OPENPAREN"

	// $ANTLR start "CLOSEPAREN"
	public final void mCLOSEPAREN() throws RecognitionException {
		try {
			int _type = CLOSEPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:25:14: ( ')' )
			// Lex.g:25:16: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLOSEPAREN"

	// $ANTLR start "ASSIGN"
	public final void mASSIGN() throws RecognitionException {
		try {
			int _type = ASSIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:26:14: ( ':=' )
			// Lex.g:26:16: ':='
			{
			match(":="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASSIGN"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:27:14: ( '+' )
			// Lex.g:27:16: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:28:14: ( '-' )
			// Lex.g:28:16: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULTIPLY"
	public final void mMULTIPLY() throws RecognitionException {
		try {
			int _type = MULTIPLY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:29:14: ( '*' )
			// Lex.g:29:16: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTIPLY"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:30:14: ( '/' )
			// Lex.g:30:16: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:35:14: ( '&' )
			// Lex.g:35:16: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "NON"
	public final void mNON() throws RecognitionException {
		try {
			int _type = NON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:36:14: ( '!' )
			// Lex.g:36:16: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NON"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:37:14: ( '||' )
			// Lex.g:37:16: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "EQUAL"
	public final void mEQUAL() throws RecognitionException {
		try {
			int _type = EQUAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:42:14: ( '=' )
			// Lex.g:42:16: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQUAL"

	// $ANTLR start "LESS"
	public final void mLESS() throws RecognitionException {
		try {
			int _type = LESS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:43:14: ( '<' )
			// Lex.g:43:16: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LESS"

	// $ANTLR start "LESSEQUAL"
	public final void mLESSEQUAL() throws RecognitionException {
		try {
			int _type = LESSEQUAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:44:14: ( '<=' )
			// Lex.g:44:16: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LESSEQUAL"

	// $ANTLR start "GREATER"
	public final void mGREATER() throws RecognitionException {
		try {
			int _type = GREATER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:45:14: ( '>' )
			// Lex.g:45:16: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GREATER"

	// $ANTLR start "GREATEREQUAL"
	public final void mGREATEREQUAL() throws RecognitionException {
		try {
			int _type = GREATEREQUAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:46:14: ( '>=' )
			// Lex.g:46:16: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GREATEREQUAL"

	// $ANTLR start "REALNUM"
	public final void mREALNUM() throws RecognitionException {
		try {
			int _type = REALNUM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:51:14: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )+ )
			// Lex.g:51:16: ( '0' .. '9' )+ '.' ( '0' .. '9' )+
			{
			// Lex.g:51:16: ( '0' .. '9' )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// Lex.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			match('.'); 
			// Lex.g:51:32: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// Lex.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "REALNUM"

	// $ANTLR start "INTNUM"
	public final void mINTNUM() throws RecognitionException {
		try {
			int _type = INTNUM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:52:14: ( ( '0' .. '9' )+ )
			// Lex.g:52:16: ( '0' .. '9' )+
			{
			// Lex.g:52:16: ( '0' .. '9' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// Lex.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTNUM"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:53:14: ( '\\'' ( '\\'' '\\'' |~ '\\'' )* '\\'' )
			// Lex.g:53:16: '\\'' ( '\\'' '\\'' |~ '\\'' )* '\\''
			{
			match('\''); 
			// Lex.g:53:21: ( '\\'' '\\'' |~ '\\'' )*
			loop4:
			while (true) {
				int alt4=3;
				int LA4_0 = input.LA(1);
				if ( (LA4_0=='\'') ) {
					int LA4_1 = input.LA(2);
					if ( (LA4_1=='\'') ) {
						alt4=1;
					}

				}
				else if ( ((LA4_0 >= '\u0000' && LA4_0 <= '&')||(LA4_0 >= '(' && LA4_0 <= '\uFFFF')) ) {
					alt4=2;
				}

				switch (alt4) {
				case 1 :
					// Lex.g:53:22: '\\'' '\\''
					{
					match('\''); 
					match('\''); 
					}
					break;
				case 2 :
					// Lex.g:53:34: ~ '\\''
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop4;
				}
			}

			match('\''); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "VARIABLE"
	public final void mVARIABLE() throws RecognitionException {
		try {
			int _type = VARIABLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			int N=1;
			// Lex.g:54:27: ( ( ( 'a' .. 'z' ) | ( 'A' .. 'Z' ) ) ( ( 'a' .. 'z' ) | ( '0' .. '9' ) | ( 'A' .. 'Z' ) )* {...}?)
			// Lex.g:54:29: ( ( 'a' .. 'z' ) | ( 'A' .. 'Z' ) ) ( ( 'a' .. 'z' ) | ( '0' .. '9' ) | ( 'A' .. 'Z' ) )* {...}?
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// Lex.g:54:55: ( ( 'a' .. 'z' ) | ( '0' .. '9' ) | ( 'A' .. 'Z' ) )*
			loop5:
			while (true) {
				int alt5=4;
				switch ( input.LA(1) ) {
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
				case 'i':
				case 'j':
				case 'k':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'p':
				case 'q':
				case 'r':
				case 's':
				case 't':
				case 'u':
				case 'v':
				case 'w':
				case 'x':
				case 'y':
				case 'z':
					{
					alt5=1;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					{
					alt5=2;
					}
					break;
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
					{
					alt5=3;
					}
					break;
				}
				switch (alt5) {
				case 1 :
					// Lex.g:54:56: ( 'a' .. 'z' )
					{
					if ( (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// Lex.g:54:71: ( '0' .. '9' )
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 3 :
					// Lex.g:54:86: ( 'A' .. 'Z' )
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					N++;
					}
					break;

				default :
					break loop5;
				}
			}

			if ( !((N<=8)) ) {
				throw new FailedPredicateException(input, "VARIABLE", "N<=8");
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VARIABLE"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:59:14: ( '{' (~ '}' )* '}' )
			// Lex.g:59:16: '{' (~ '}' )* '}'
			{
			match('{'); 
			// Lex.g:59:20: (~ '}' )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( ((LA6_0 >= '\u0000' && LA6_0 <= '|')||(LA6_0 >= '~' && LA6_0 <= '\uFFFF')) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// Lex.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '|')||(input.LA(1) >= '~' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop6;
				}
			}

			match('}'); 
			skip();
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// Lex.g:60:14: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
			// Lex.g:60:16: ( ' ' | '\\t' | '\\r' | '\\n' )+
			{
			// Lex.g:60:16: ( ' ' | '\\t' | '\\r' | '\\n' )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( ((LA7_0 >= '\t' && LA7_0 <= '\n')||LA7_0=='\r'||LA7_0==' ') ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// Lex.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}

			skip();
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// Lex.g:1:8: ( DO | ELSE | FALSE | IF | READ | SKIP | THEN | TRUE | WHILE | WRITE | WRITELN | SEMICOLON | OPENPAREN | CLOSEPAREN | ASSIGN | PLUS | MINUS | MULTIPLY | DIV | AND | NON | OR | EQUAL | LESS | LESSEQUAL | GREATER | GREATEREQUAL | REALNUM | INTNUM | STRING | VARIABLE | COMMENT | WS )
		int alt8=33;
		alt8 = dfa8.predict(input);
		switch (alt8) {
			case 1 :
				// Lex.g:1:10: DO
				{
				mDO(); 

				}
				break;
			case 2 :
				// Lex.g:1:13: ELSE
				{
				mELSE(); 

				}
				break;
			case 3 :
				// Lex.g:1:18: FALSE
				{
				mFALSE(); 

				}
				break;
			case 4 :
				// Lex.g:1:24: IF
				{
				mIF(); 

				}
				break;
			case 5 :
				// Lex.g:1:27: READ
				{
				mREAD(); 

				}
				break;
			case 6 :
				// Lex.g:1:32: SKIP
				{
				mSKIP(); 

				}
				break;
			case 7 :
				// Lex.g:1:37: THEN
				{
				mTHEN(); 

				}
				break;
			case 8 :
				// Lex.g:1:42: TRUE
				{
				mTRUE(); 

				}
				break;
			case 9 :
				// Lex.g:1:47: WHILE
				{
				mWHILE(); 

				}
				break;
			case 10 :
				// Lex.g:1:53: WRITE
				{
				mWRITE(); 

				}
				break;
			case 11 :
				// Lex.g:1:59: WRITELN
				{
				mWRITELN(); 

				}
				break;
			case 12 :
				// Lex.g:1:67: SEMICOLON
				{
				mSEMICOLON(); 

				}
				break;
			case 13 :
				// Lex.g:1:77: OPENPAREN
				{
				mOPENPAREN(); 

				}
				break;
			case 14 :
				// Lex.g:1:87: CLOSEPAREN
				{
				mCLOSEPAREN(); 

				}
				break;
			case 15 :
				// Lex.g:1:98: ASSIGN
				{
				mASSIGN(); 

				}
				break;
			case 16 :
				// Lex.g:1:105: PLUS
				{
				mPLUS(); 

				}
				break;
			case 17 :
				// Lex.g:1:110: MINUS
				{
				mMINUS(); 

				}
				break;
			case 18 :
				// Lex.g:1:116: MULTIPLY
				{
				mMULTIPLY(); 

				}
				break;
			case 19 :
				// Lex.g:1:125: DIV
				{
				mDIV(); 

				}
				break;
			case 20 :
				// Lex.g:1:129: AND
				{
				mAND(); 

				}
				break;
			case 21 :
				// Lex.g:1:133: NON
				{
				mNON(); 

				}
				break;
			case 22 :
				// Lex.g:1:137: OR
				{
				mOR(); 

				}
				break;
			case 23 :
				// Lex.g:1:140: EQUAL
				{
				mEQUAL(); 

				}
				break;
			case 24 :
				// Lex.g:1:146: LESS
				{
				mLESS(); 

				}
				break;
			case 25 :
				// Lex.g:1:151: LESSEQUAL
				{
				mLESSEQUAL(); 

				}
				break;
			case 26 :
				// Lex.g:1:161: GREATER
				{
				mGREATER(); 

				}
				break;
			case 27 :
				// Lex.g:1:169: GREATEREQUAL
				{
				mGREATEREQUAL(); 

				}
				break;
			case 28 :
				// Lex.g:1:182: REALNUM
				{
				mREALNUM(); 

				}
				break;
			case 29 :
				// Lex.g:1:190: INTNUM
				{
				mINTNUM(); 

				}
				break;
			case 30 :
				// Lex.g:1:197: STRING
				{
				mSTRING(); 

				}
				break;
			case 31 :
				// Lex.g:1:204: VARIABLE
				{
				mVARIABLE(); 

				}
				break;
			case 32 :
				// Lex.g:1:213: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 33 :
				// Lex.g:1:221: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA8 dfa8 = new DFA8(this);
	static final String DFA8_eotS =
		"\1\uffff\10\31\14\uffff\1\47\1\51\1\53\4\uffff\1\54\2\31\1\57\6\31\7\uffff"+
		"\2\31\1\uffff\6\31\1\76\1\31\1\100\1\101\1\102\1\103\2\31\1\uffff\1\106"+
		"\4\uffff\1\107\1\111\2\uffff\1\31\1\uffff\1\113\1\uffff";
	static final String DFA8_eofS =
		"\114\uffff";
	static final String DFA8_minS =
		"\1\11\1\157\1\154\1\141\1\146\1\145\1\153\2\150\14\uffff\2\75\1\56\4\uffff"+
		"\1\60\1\163\1\154\1\60\1\141\1\151\1\145\1\165\2\151\7\uffff\1\145\1\163"+
		"\1\uffff\1\144\1\160\1\156\1\145\1\154\1\164\1\60\1\145\4\60\2\145\1\uffff"+
		"\1\60\4\uffff\2\60\2\uffff\1\156\1\uffff\1\60\1\uffff";
	static final String DFA8_maxS =
		"\1\174\1\157\1\154\1\141\1\146\1\145\1\153\2\162\14\uffff\2\75\1\71\4"+
		"\uffff\1\172\1\163\1\154\1\172\1\141\1\151\1\145\1\165\2\151\7\uffff\1"+
		"\145\1\163\1\uffff\1\144\1\160\1\156\1\145\1\154\1\164\1\172\1\145\4\172"+
		"\2\145\1\uffff\1\172\4\uffff\2\172\2\uffff\1\156\1\uffff\1\172\1\uffff";
	static final String DFA8_acceptS =
		"\11\uffff\1\14\1\15\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27"+
		"\3\uffff\1\36\1\37\1\40\1\41\12\uffff\1\31\1\30\1\33\1\32\1\34\1\35\1"+
		"\1\2\uffff\1\4\16\uffff\1\2\1\uffff\1\5\1\6\1\7\1\10\2\uffff\1\3\1\11"+
		"\1\uffff\1\12\1\uffff\1\13";
	static final String DFA8_specialS =
		"\114\uffff}>";
	static final String[] DFA8_transitionS = {
			"\2\33\2\uffff\1\33\22\uffff\1\33\1\22\4\uffff\1\21\1\30\1\12\1\13\1\17"+
			"\1\15\1\uffff\1\16\1\uffff\1\20\12\27\1\14\1\11\1\25\1\24\1\26\2\uffff"+
			"\32\31\6\uffff\3\31\1\1\1\2\1\3\2\31\1\4\10\31\1\5\1\6\1\7\2\31\1\10"+
			"\3\31\1\32\1\23",
			"\1\34",
			"\1\35",
			"\1\36",
			"\1\37",
			"\1\40",
			"\1\41",
			"\1\42\11\uffff\1\43",
			"\1\44\11\uffff\1\45",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\46",
			"\1\50",
			"\1\52\1\uffff\12\27",
			"",
			"",
			"",
			"",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\1\55",
			"\1\56",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\1\60",
			"\1\61",
			"\1\62",
			"\1\63",
			"\1\64",
			"\1\65",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\66",
			"\1\67",
			"",
			"\1\70",
			"\1\71",
			"\1\72",
			"\1\73",
			"\1\74",
			"\1\75",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\1\77",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\1\104",
			"\1\105",
			"",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"",
			"",
			"",
			"",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			"\12\31\7\uffff\32\31\6\uffff\13\31\1\110\16\31",
			"",
			"",
			"\1\112",
			"",
			"\12\31\7\uffff\32\31\6\uffff\32\31",
			""
	};

	static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
	static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
	static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
	static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
	static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
	static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
	static final short[][] DFA8_transition;

	static {
		int numStates = DFA8_transitionS.length;
		DFA8_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
		}
	}

	protected class DFA8 extends DFA {

		public DFA8(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 8;
			this.eot = DFA8_eot;
			this.eof = DFA8_eof;
			this.min = DFA8_min;
			this.max = DFA8_max;
			this.accept = DFA8_accept;
			this.special = DFA8_special;
			this.transition = DFA8_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( DO | ELSE | FALSE | IF | READ | SKIP | THEN | TRUE | WHILE | WRITE | WRITELN | SEMICOLON | OPENPAREN | CLOSEPAREN | ASSIGN | PLUS | MINUS | MULTIPLY | DIV | AND | NON | OR | EQUAL | LESS | LESSEQUAL | GREATER | GREATEREQUAL | REALNUM | INTNUM | STRING | VARIABLE | COMMENT | WS );";
		}
	}

}
