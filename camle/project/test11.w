{COMS22201 test10: program to test DIV(/) operator.}
{Already tested: write statements, variable assignment, read statement, real numbers and constants.}

write('Division calculator ');
writeln;
write('Enter the first integer : ');
read(i);
write('Enter the second integer different than 0 : ');
read(j);
writeln;
write('The result ');
write(i);
write(' divided by ');
write(j);
write(' is : ');
k := i / j;
write(k);
writeln;
write(2.5/0.4);
writeln
