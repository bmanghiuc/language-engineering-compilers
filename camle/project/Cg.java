// COMS22201: Code generation

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class Cg
{
  private static int intermediateLabelNumber = 0;
  private static int registerCounter = 1;
  private static String[] numberType = new String[1];
  public static void program(IRTree irt, PrintStream o)
  {
    emit(o, "XOR R0,R0,R0");   // Initialize R0 to 0
    statement(irt, o);
    emit(o, "HALT");           // Program must end with HALT
    Memory.dumpData(o);        // Dump DATA lines: initial memory contents
  }

  private static void statement(IRTree irt, PrintStream o)
  {
    if (irt.getOp().equals("SEQ")) {
      statement(irt.getSub(0), o);
      registerCounter = 1;
      statement(irt.getSub(1), o);
    }
    else if (irt.getOp().equals("WRS") && (irt.getSub(0).getOp().equals("MEM") && irt.getSub(0).getSub(0).getOp().equals("CONST"))) {
      String a = irt.getSub(0).getSub(0).getSub(0).getOp();
      emit(o, "WRS "+a);
    }
    else if (irt.getOp().equals("WRR")){
      String e = expression(irt.getSub(0),o, registerCounter, numberType);
      emit(o, "WRR " +e);
    }
    else if (irt.getOp().equals("WR")) {
      String e = expression(irt.getSub(0), o, registerCounter, numberType);
      emit(o, "WR " +e);
    }
    else if(irt.getOp().equals("WRB")){
     String value = evaluateBoolean(irt.getSub(0));
     emit(o, value);
    }
    else if (irt.getOp().equals("MOVE") && irt.getSub(0).getOp().equals("MEM")){
      if(irt.getSub(1).getOp().equals("CONST")){
	 store(irt.getSub(0), irt.getSub(1), o);
      }
      else if(irt.getSub(1).getOp().equals("MEM")){
         move(irt.getSub(0), irt.getSub(1), o);
      }
      else if(irt.getSub(1).getOp().equals("BINOP")){
         String result = expression(irt.getSub(1), o, registerCounter, numberType);
         store(irt.getSub(0), result, o);
      } 
    }
    else if (irt.getOp().equals("RD") && irt.getSub(0).getOp().equals("MEM")){
      store(irt.getSub(0),true, o);
    }
    else if(irt.getOp().equals("RDR") && irt.getSub(0).getOp().equals("MEM")){
      store(irt.getSub(0),false, o);
    }
    else if (irt.getOp().equals("CJUMP")){
      evaluateBooleanExpression(o,irt.getSub(0).getSub(0), irt.getSub(1).getOp(), irt.getSub(2).getOp()); 
    }
    else if(irt.getOp().equals("LABEL")){
      emit(o,irt.getSub(0) + ":");
    }
    else if(irt.getOp().equals("JUMP")){
      emit(o,"JMP " + irt.getSub(0));
    }
    else if(irt.getOp().equals("NOP")){
      emit(o,"NOP");
    }
    else{
      error(irt.getOp());
    }
  }
  private static void evaluateBooleanExpression(PrintStream o,IRTree booleanExpression, String label1, String label2)
  {
    String[] type1 = new String[5];
    String[] type2 = new String[5];
    if(booleanExpression.getOp().equals("true")){
      emit(o, "JMP " + label1);
    }
    else if(booleanExpression.getOp().equals("false")){
      emit(o, "JMP " + label2);
    }
    else if(booleanExpression.getOp().equals("LESS")){
      String e1 = expression(booleanExpression.getSub(0), o, ++registerCounter, type1);
      String e2 = expression(booleanExpression.getSub(1), o, ++registerCounter, type2);
      if(type1[0].equals("int")){
      	emit(o, "SUB R" + (++registerCounter) + "," + e1 + "," +e2);
      	emit(o,"BLTZ R" + registerCounter + "," + label1);
     	emit(o,"BEQZ R" + registerCounter + "," + label2);
      	emit(o,"BGEZ R" + registerCounter + "," + label2);
      }
      else {
        emit(o, "SUBR R" + (++registerCounter) + "," + e1 + "," +e2);
      	emit(o,"BLTZR R" + registerCounter + "," + label1);
     	emit(o,"BEQZR R" + registerCounter + "," + label2);
      	emit(o,"BGEZR R" + registerCounter + "," + label2);
      }
    }
    else if(booleanExpression.getOp().equals("LESSEQUAL")){
      String e1 = expression(booleanExpression.getSub(0), o, ++registerCounter, type1);
      String e2 = expression(booleanExpression.getSub(1), o, ++registerCounter, type2);
     
      if(type1[0].equals("int")){
      	emit(o, "SUB R" + (++registerCounter) + "," + e1 + "," +e2);
      	emit(o,"BLTZ R" + registerCounter + "," + label1);
      	emit(o,"BEQZ R" + registerCounter + "," + label1);
      	emit(o,"BGEZ R" + registerCounter + "," + label2);
      }
      else {
        emit(o, "SUBR R" + (++registerCounter) + "," + e1 + "," +e2);
      	emit(o,"BLTZR R" + registerCounter + "," + label1);
      	emit(o,"BEQZR R" + registerCounter + "," + label1);
      	emit(o,"BGEZR R" + registerCounter + "," + label2);
      }
    }
    else if(booleanExpression.getOp().equals("EQUAL")){
      String e1 = expression(booleanExpression.getSub(0), o, ++registerCounter, type1);
      String e2 = expression(booleanExpression.getSub(1), o, ++registerCounter, type2);
      if(type1[0].equals("int")){
        emit(o, "SUB R" + (++registerCounter) + "," + e1 + "," +e2);
        emit(o,"BEQZ R" + registerCounter + "," + label1);
        emit(o,"BLTZ R" + registerCounter + "," + label2);
        emit(o,"BGEZ R" + registerCounter + "," + label2);
      }
      else {
        emit(o, "SUBR R" + (++registerCounter) + "," + e1 + "," +e2);
        emit(o,"BEQZR R" + registerCounter + "," + label1);
        emit(o,"BLTZR R" + registerCounter + "," + label2);
        emit(o,"BGEZR R" + registerCounter + "," + label2);
      }
    }
    else if(booleanExpression.getOp().equals("GREATEREQUAL")){
      String e1 = expression(booleanExpression.getSub(0), o, ++registerCounter, type1);
      String e2 = expression(booleanExpression.getSub(1), o, ++registerCounter, type2);
      if(type1[0].equals("int")){
        emit(o, "SUB R" + (++registerCounter) + "," + e1 + "," +e2);
        emit(o,"BGEZ R" + registerCounter + "," + label1);
        emit(o,"BLTZ R" + registerCounter + "," + label2);
      }
      else {
        emit(o, "SUBR R" + (++registerCounter) + "," + e1 + "," +e2);
        emit(o,"BGEZR R" + registerCounter + "," + label1);
        emit(o,"BLTZR R" + registerCounter + "," + label2);
      }
      
    }
    else if(booleanExpression.getOp().equals("GREATER")){
      String e1 = expression(booleanExpression.getSub(0), o, ++registerCounter, type1);
      String e2 = expression(booleanExpression.getSub(1), o, ++registerCounter, type2);
      if(type1[0].equals("int")){
        emit(o, "SUB R" + (++registerCounter) + "," + e1 + "," +e2);
        emit(o,"BEQZ R" + registerCounter + "," + label2);
        emit(o,"BGEZ R" + registerCounter + "," + label1);
        emit(o,"BLTZ R" + registerCounter + "," + label2);
      }
      else {
        emit(o, "SUBR R" + (++registerCounter) + "," + e1 + "," +e2);
        emit(o,"BEQZR R" + registerCounter + "," + label2);
        emit(o,"BGEZR R" + registerCounter + "," + label1);
        emit(o,"BLTZR R" + registerCounter + "," + label2);
      }
      
    }
    else if(booleanExpression.getOp().equals("AND")){
      int t = intermediateLabelNumber;
      evaluateBooleanExpression(o, booleanExpression.getSub(0), "I" + (++intermediateLabelNumber), label2);
      emit(o, "I" + (t+1) + ":");
      evaluateBooleanExpression(o, booleanExpression.getSub(1), label1, label2);
    }
    else if(booleanExpression.getOp().equals("OR")){
      int t = intermediateLabelNumber;
      evaluateBooleanExpression(o, booleanExpression.getSub(0), label1, "I" + (++intermediateLabelNumber));
      emit(o, "I" + (t+1) + ":");
      evaluateBooleanExpression(o, booleanExpression.getSub(1), label1, label2);
    }
    else if(booleanExpression.getOp().equals("NON")){
      evaluateBooleanExpression(o, booleanExpression.getSub(0), label2, label1);
    }
    else {
      error(booleanExpression.getOp());
    }
  }
  
  private static String evaluateBoolean(IRTree irt){
    if(irt.getOp().equals("true"))
      return "true";
    else if(irt.getOp().equals("false"))
      return "false";
    else if(irt.getOp().equals("NON")){
      String child = evaluateBoolean(irt.getSub(0));
      if(child.equals("true")) return "false";
      else if(child.equals("false")) return "true";
    }
    else if(irt.getOp().equals("AND")){
      String child1 = evaluateBoolean(irt.getSub(0)), child2 = evaluateBoolean(irt.getSub(1));
      if(child1.equals("false") || child2.equals("false")) return "false";
      else return "true";
    }
    else{
      error(irt.getOp());
    }
    return "";
  }
  private static String expression(IRTree irt, PrintStream o, int registerCounter, String[] type)
  {
    String[] t1 = new String[5];
    String[] t2 = new String[5];
    String result = "";
    if (irt.getOp().equals("CONST")) {
      String t = irt.getSub(0).getOp();
      if(!(t.indexOf('.') > 0)){
      	result = "R" + registerCounter;
      	emit(o, "ADDI "+result+",R0,"+t);
        type[0] = "int";
      }
      else {
	result = "R" + registerCounter;
	emit(o, "MOVIR "+result +"," +t);
        type[0] = "real";
      }
    }
    else if(irt.getOp().equals("MEM")){
      String t = irt.getSub(0).getSub(0).getOp();
      result = "R" + registerCounter;
      emit(o, "LOAD "+result+",R0,"+t);
      if(Memory.memory.get(Integer.parseInt(t)).getContents() == 2)
        type[0] = "real";
      else
        type[0] = "int";
    }
    else if(irt.getOp().equals("BINOP")){
      result = "R" + registerCounter ;
      expression(irt.getSub(1), o, ++registerCounter, t1);
      expression(irt.getSub(2), o, ++registerCounter, t2);
      if(t1[0].equals(t2[0])){
        type[0]= t1[0];
      }
      if(irt.getSub(0).getOp().equals("+real")){
        emit(o, "ADDR " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter); 
      }
      else if(irt.getSub(0).getOp().equals("+int")){
        emit(o, "ADD " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter); 
      }
      else if(irt.getSub(0).getOp().equals("-int")){
	emit(o, "SUB " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter);
      }
      else if(irt.getSub(0).getOp().equals("-real")){
	emit(o, "SUBR " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter);
      }
      else if(irt.getSub(0).getOp().equals("*int")){
        emit(o, "MUL " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter);
      }
      else if(irt.getSub(0).getOp().equals("*real")){
        emit(o, "MULR " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter);
      }
      else if(irt.getSub(0).getOp().equals("/int")){
        emit(o, "DIV " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter);
      }
      else if(irt.getSub(0).getOp().equals("/real")){
        emit(o, "DIVR " + "R" + (registerCounter - 2) + ",R" + (registerCounter - 1) + ",R" + registerCounter);
      }
      else{
        error(irt.getSub(0).getOp());
      }        
    }
    else {
      error(irt.getOp());
    }
    return result;
  }
  private static void move(IRTree irt1, IRTree irt2, PrintStream o)
  {
    if(irt2.getOp().equals("MEM")) {
      String t = irt2.getSub(0).getSub(0).getOp();
      emit(o, "LOAD " +"R1,R0," +t);
      String memVal = irt1.getSub(0).getSub(0).getOp();
      emit(o, "STORE " +"R1,R0," + memVal);
    }
    else {
       error("MOVE");
    }
  }
  private static void store(IRTree irt1, IRTree irt2, PrintStream o)
  {
    if(irt2.getOp().equals("CONST")) {
      String t = irt2.getSub(0).getOp();
      if(t.indexOf('.') < 0){
        emit(o, "ADDI " +"R1,R0," +t);
      }
      else {
        emit(o, "MOVIR " +"R1," +t);
      }
      String memVal = irt1.getSub(0).getSub(0).getOp();
      emit(o, "STORE " +"R1,R0," + memVal);
    }
    else {
      error("MOVE");
    }
  }
  private static void store(IRTree irt1, boolean type, PrintStream o)
  {
    String memVal = irt1.getSub(0).getSub(0).getOp();
    if(type == true)
      emit(o, "RD R1");
    else
      emit(o, "RDR R1");      
    emit(o, "STORE " +"R1,R0," + memVal);
  }
  private static void store(IRTree irt1, String register, PrintStream o)
  {
    String memVal = irt1.getSub(0).getSub(0).getOp();
    emit(o, "STORE " + register + ",R0," + memVal);
  }

  private static void emit(PrintStream o, String s)
  {
    o.println(s);
  }

  private static void error(String op)
  {
    System.out.println("CG error: "+op);
    System.exit(1);
  }
}
