// COMS22201: Memory allocation for strings

import java.util.ArrayList;
import java.util.*;
import java.io.*;

public class Memory {

  static ArrayList<Byte> memory = new ArrayList<Byte>();
  static Map<String, Integer> allocation = new HashMap<String, Integer>();
  static Map<String, Integer> booleanAllocation = new HashMap<String, Integer>();

  static public int allocateString(String text)
  {
    int addr = memory.size();
    int size = text.length();
    if((text.equals("true") || text.equals("false")))
    {
      if((!booleanAllocation.containsKey(text))){
      	for (int i=0; i<size; i++){
      	  memory.add(new Byte("", text.charAt(i)));
        }
      booleanAllocation.put(text, addr);
      }
      else {
        return booleanAllocation.get(text);
      }
    }
    else {
      for (int i=0; i<size; i++){
        memory.add(new Byte("", text.charAt(i)));
      }
    }
    memory.add(new Byte("", 0));
    return addr;
  }
  static public int allocateVariable(String text, String type)
  {
    int addr = memory.size();
    if(! allocation.containsKey(text)){
      int c = addr %4;
      for(int i=c; i<4; i++)
      {
        addr++;
        memory.add(new Byte("",0));
      }
      int size = 4;
      int varType = 0;
      if(type.equals("int"))
        varType = 1;
      else
        varType = 2;
      for( int i=0; i<size; i++) {
        memory.add(new Byte(text, varType));
      }
      allocation.put(text, addr);
      return addr;
    }
    else {
      return allocation.get(text);
    }
  }
  static public int allocateVariable(String text)
  {
    int addr = memory.size();
    if(! allocation.containsKey(text)){
      int c = addr %4;
      for(int i=c; i<4; i++)
      {
        addr++;
        memory.add(new Byte("",0));
      }
      int size = 4;
      for( int i=0; i<size; i++) {
        memory.add(new Byte(text, 0));
      }
      allocation.put(text, addr);
      return addr;
    }
    else {
      return allocation.get(text);
    }
  }

  static public void dumpData(PrintStream o)
  {
    Byte b;
    String s;
    int c;
    int size = memory.size();
    for (int i=0; i<size; i++) {
      b = memory.get(i);
      c = b.getContents();
      if (c >= 32) {
        s = String.valueOf((char)c);
      }
      else {
        s = ""; // "\\"+String.valueOf(c);
      }
      o.println("DATA "+c+" ; "+s+" "+b.getName());
    }
  }
}

class Byte {
  String varname;
  int contents;

  Byte(String n, int c)
  {
    varname = n;
    contents = c;
  }

  String getName()
  {
    return varname;
  }

  int getContents()
  {
    return contents;
  }
}
