{COMS22201 test9: program to test real numbers}
{Already tested: write statements, constants, variables, assignment, if statements, while loops, arithmetic expressions.}
{-----------------------------------------------------------------------------}
write('****Test assignment of variables and writing a floating point number****');
writeln;
i := 7.5;
write(i);
writeln;
a := i;
write(a);
writeln;
j := 6.8;
write(j);
writeln;
a := j;
write(a);
writeln;
{-----------------------------------------------------------------------------}
write('****Test expressions with real numbers****');
writeln;
write(4.6+3.2);
writeln;
write(0.6-51.9);
writeln;
write(4.23*0.7);
writeln;
write(1.9+2.1*(0.7-6.2)-20.19*4.56);
writeln;
write(2.7*(25.12+5.8)+3.9*(6.7)-2.5*4.555);
writeln;
{-----------------------------------------------------------------------------}
write('****Test the if statement****');
writeln;
if 1.3 + 0.7 > 2.6-0.7
  then write(a)
  else skip;
writeln;
if 5.6 < 5.6
  then ( write(5); writeln; writeln )
  else ( write(6); writeln; writeln );
{-----------------------------------------------------------------------------}
write('****Test the while statement****');
writeln;
i := 1.5;
while(i <= 3.5) do
(
  write('After iteration ');
  write(i - 0.5);
  write(' print ');
  write(i);
  i := i+1.0;
  writeln
);
writeln
