// $ANTLR 3.5.2 Syn.g 2015-12-16 17:01:49

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class Syn extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ASSIGN", "CLOSEPAREN", 
		"COMMENT", "DIV", "DO", "ELSE", "EQUAL", "FALSE", "GREATER", "GREATEREQUAL", 
		"IF", "INTNUM", "LESS", "LESSEQUAL", "MINUS", "MULTIPLY", "NON", "OPENPAREN", 
		"OR", "PLUS", "READ", "REALNUM", "SEMICOLON", "SKIP", "STRING", "THEN", 
		"TRUE", "VARIABLE", "WHILE", "WRITE", "WRITELN", "WS"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ASSIGN=5;
	public static final int CLOSEPAREN=6;
	public static final int COMMENT=7;
	public static final int DIV=8;
	public static final int DO=9;
	public static final int ELSE=10;
	public static final int EQUAL=11;
	public static final int FALSE=12;
	public static final int GREATER=13;
	public static final int GREATEREQUAL=14;
	public static final int IF=15;
	public static final int INTNUM=16;
	public static final int LESS=17;
	public static final int LESSEQUAL=18;
	public static final int MINUS=19;
	public static final int MULTIPLY=20;
	public static final int NON=21;
	public static final int OPENPAREN=22;
	public static final int OR=23;
	public static final int PLUS=24;
	public static final int READ=25;
	public static final int REALNUM=26;
	public static final int SEMICOLON=27;
	public static final int SKIP=28;
	public static final int STRING=29;
	public static final int THEN=30;
	public static final int TRUE=31;
	public static final int VARIABLE=32;
	public static final int WHILE=33;
	public static final int WRITE=34;
	public static final int WRITELN=35;
	public static final int WS=36;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public Syn(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public Syn(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return Syn.tokenNames; }
	@Override public String getGrammarFileName() { return "Syn.g"; }


		private String cleanString(String s){
			String tmp;
			tmp = s.replaceAll("^'", "");
			s = tmp.replaceAll("'$", "");
			tmp = s.replaceAll("''", "'");
			return tmp;
		}


	public static class program_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "program"
	// Syn.g:22:1: program : statements ;
	public final Syn.program_return program() throws RecognitionException {
		Syn.program_return retval = new Syn.program_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope statements1 =null;


		try {
			// Syn.g:22:9: ( statements )
			// Syn.g:23:5: statements
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_statements_in_program56);
			statements1=statements();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, statements1.getTree());

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "program"


	public static class statements_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "statements"
	// Syn.g:26:1: statements : statement ( SEMICOLON ^ statement )* ;
	public final Syn.statements_return statements() throws RecognitionException {
		Syn.statements_return retval = new Syn.statements_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SEMICOLON3=null;
		ParserRuleReturnScope statement2 =null;
		ParserRuleReturnScope statement4 =null;

		Object SEMICOLON3_tree=null;

		try {
			// Syn.g:26:12: ( statement ( SEMICOLON ^ statement )* )
			// Syn.g:27:5: statement ( SEMICOLON ^ statement )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_statement_in_statements71);
			statement2=statement();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, statement2.getTree());

			// Syn.g:27:15: ( SEMICOLON ^ statement )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==SEMICOLON) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// Syn.g:27:17: SEMICOLON ^ statement
					{
					SEMICOLON3=(Token)match(input,SEMICOLON,FOLLOW_SEMICOLON_in_statements75); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SEMICOLON3_tree = (Object)adaptor.create(SEMICOLON3);
					root_0 = (Object)adaptor.becomeRoot(SEMICOLON3_tree, root_0);
					}

					pushFollow(FOLLOW_statement_in_statements78);
					statement4=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement4.getTree());

					}
					break;

				default :
					break loop1;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statements"


	public static class statement_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "statement"
	// Syn.g:30:1: statement : ( WRITE ^ content | WRITELN ^| READ ^ OPENPAREN ! VARIABLE CLOSEPAREN !| VARIABLE ASSIGN ^ expression | IF ^ boolexp THEN ! statement ELSE ! statement | WHILE ^ boolexp DO ! statement | OPENPAREN ! statements CLOSEPAREN !| SKIP ^);
	public final Syn.statement_return statement() throws RecognitionException {
		Syn.statement_return retval = new Syn.statement_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token WRITE5=null;
		Token WRITELN7=null;
		Token READ8=null;
		Token OPENPAREN9=null;
		Token VARIABLE10=null;
		Token CLOSEPAREN11=null;
		Token VARIABLE12=null;
		Token ASSIGN13=null;
		Token IF15=null;
		Token THEN17=null;
		Token ELSE19=null;
		Token WHILE21=null;
		Token DO23=null;
		Token OPENPAREN25=null;
		Token CLOSEPAREN27=null;
		Token SKIP28=null;
		ParserRuleReturnScope content6 =null;
		ParserRuleReturnScope expression14 =null;
		ParserRuleReturnScope boolexp16 =null;
		ParserRuleReturnScope statement18 =null;
		ParserRuleReturnScope statement20 =null;
		ParserRuleReturnScope boolexp22 =null;
		ParserRuleReturnScope statement24 =null;
		ParserRuleReturnScope statements26 =null;

		Object WRITE5_tree=null;
		Object WRITELN7_tree=null;
		Object READ8_tree=null;
		Object OPENPAREN9_tree=null;
		Object VARIABLE10_tree=null;
		Object CLOSEPAREN11_tree=null;
		Object VARIABLE12_tree=null;
		Object ASSIGN13_tree=null;
		Object IF15_tree=null;
		Object THEN17_tree=null;
		Object ELSE19_tree=null;
		Object WHILE21_tree=null;
		Object DO23_tree=null;
		Object OPENPAREN25_tree=null;
		Object CLOSEPAREN27_tree=null;
		Object SKIP28_tree=null;

		try {
			// Syn.g:30:11: ( WRITE ^ content | WRITELN ^| READ ^ OPENPAREN ! VARIABLE CLOSEPAREN !| VARIABLE ASSIGN ^ expression | IF ^ boolexp THEN ! statement ELSE ! statement | WHILE ^ boolexp DO ! statement | OPENPAREN ! statements CLOSEPAREN !| SKIP ^)
			int alt2=8;
			switch ( input.LA(1) ) {
			case WRITE:
				{
				alt2=1;
				}
				break;
			case WRITELN:
				{
				alt2=2;
				}
				break;
			case READ:
				{
				alt2=3;
				}
				break;
			case VARIABLE:
				{
				alt2=4;
				}
				break;
			case IF:
				{
				alt2=5;
				}
				break;
			case WHILE:
				{
				alt2=6;
				}
				break;
			case OPENPAREN:
				{
				alt2=7;
				}
				break;
			case SKIP:
				{
				alt2=8;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}
			switch (alt2) {
				case 1 :
					// Syn.g:31:5: WRITE ^ content
					{
					root_0 = (Object)adaptor.nil();


					WRITE5=(Token)match(input,WRITE,FOLLOW_WRITE_in_statement96); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					WRITE5_tree = (Object)adaptor.create(WRITE5);
					root_0 = (Object)adaptor.becomeRoot(WRITE5_tree, root_0);
					}

					pushFollow(FOLLOW_content_in_statement99);
					content6=content();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, content6.getTree());

					}
					break;
				case 2 :
					// Syn.g:32:5: WRITELN ^
					{
					root_0 = (Object)adaptor.nil();


					WRITELN7=(Token)match(input,WRITELN,FOLLOW_WRITELN_in_statement105); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					WRITELN7_tree = (Object)adaptor.create(WRITELN7);
					root_0 = (Object)adaptor.becomeRoot(WRITELN7_tree, root_0);
					}

					}
					break;
				case 3 :
					// Syn.g:33:5: READ ^ OPENPAREN ! VARIABLE CLOSEPAREN !
					{
					root_0 = (Object)adaptor.nil();


					READ8=(Token)match(input,READ,FOLLOW_READ_in_statement112); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					READ8_tree = (Object)adaptor.create(READ8);
					root_0 = (Object)adaptor.becomeRoot(READ8_tree, root_0);
					}

					OPENPAREN9=(Token)match(input,OPENPAREN,FOLLOW_OPENPAREN_in_statement115); if (state.failed) return retval;
					VARIABLE10=(Token)match(input,VARIABLE,FOLLOW_VARIABLE_in_statement118); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					VARIABLE10_tree = (Object)adaptor.create(VARIABLE10);
					adaptor.addChild(root_0, VARIABLE10_tree);
					}

					CLOSEPAREN11=(Token)match(input,CLOSEPAREN,FOLLOW_CLOSEPAREN_in_statement120); if (state.failed) return retval;
					}
					break;
				case 4 :
					// Syn.g:34:5: VARIABLE ASSIGN ^ expression
					{
					root_0 = (Object)adaptor.nil();


					VARIABLE12=(Token)match(input,VARIABLE,FOLLOW_VARIABLE_in_statement127); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					VARIABLE12_tree = (Object)adaptor.create(VARIABLE12);
					adaptor.addChild(root_0, VARIABLE12_tree);
					}

					ASSIGN13=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_statement129); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ASSIGN13_tree = (Object)adaptor.create(ASSIGN13);
					root_0 = (Object)adaptor.becomeRoot(ASSIGN13_tree, root_0);
					}

					pushFollow(FOLLOW_expression_in_statement132);
					expression14=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression14.getTree());

					}
					break;
				case 5 :
					// Syn.g:35:5: IF ^ boolexp THEN ! statement ELSE ! statement
					{
					root_0 = (Object)adaptor.nil();


					IF15=(Token)match(input,IF,FOLLOW_IF_in_statement138); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					IF15_tree = (Object)adaptor.create(IF15);
					root_0 = (Object)adaptor.becomeRoot(IF15_tree, root_0);
					}

					pushFollow(FOLLOW_boolexp_in_statement141);
					boolexp16=boolexp();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, boolexp16.getTree());

					THEN17=(Token)match(input,THEN,FOLLOW_THEN_in_statement143); if (state.failed) return retval;
					pushFollow(FOLLOW_statement_in_statement146);
					statement18=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement18.getTree());

					ELSE19=(Token)match(input,ELSE,FOLLOW_ELSE_in_statement148); if (state.failed) return retval;
					pushFollow(FOLLOW_statement_in_statement151);
					statement20=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement20.getTree());

					}
					break;
				case 6 :
					// Syn.g:36:5: WHILE ^ boolexp DO ! statement
					{
					root_0 = (Object)adaptor.nil();


					WHILE21=(Token)match(input,WHILE,FOLLOW_WHILE_in_statement157); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					WHILE21_tree = (Object)adaptor.create(WHILE21);
					root_0 = (Object)adaptor.becomeRoot(WHILE21_tree, root_0);
					}

					pushFollow(FOLLOW_boolexp_in_statement160);
					boolexp22=boolexp();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, boolexp22.getTree());

					DO23=(Token)match(input,DO,FOLLOW_DO_in_statement162); if (state.failed) return retval;
					pushFollow(FOLLOW_statement_in_statement165);
					statement24=statement();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statement24.getTree());

					}
					break;
				case 7 :
					// Syn.g:37:5: OPENPAREN ! statements CLOSEPAREN !
					{
					root_0 = (Object)adaptor.nil();


					OPENPAREN25=(Token)match(input,OPENPAREN,FOLLOW_OPENPAREN_in_statement171); if (state.failed) return retval;
					pushFollow(FOLLOW_statements_in_statement174);
					statements26=statements();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, statements26.getTree());

					CLOSEPAREN27=(Token)match(input,CLOSEPAREN,FOLLOW_CLOSEPAREN_in_statement176); if (state.failed) return retval;
					}
					break;
				case 8 :
					// Syn.g:38:5: SKIP ^
					{
					root_0 = (Object)adaptor.nil();


					SKIP28=(Token)match(input,SKIP,FOLLOW_SKIP_in_statement183); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					SKIP28_tree = (Object)adaptor.create(SKIP28);
					root_0 = (Object)adaptor.becomeRoot(SKIP28_tree, root_0);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class content_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "content"
	// Syn.g:41:1: content : OPENPAREN ! argument CLOSEPAREN !;
	public final Syn.content_return content() throws RecognitionException {
		Syn.content_return retval = new Syn.content_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token OPENPAREN29=null;
		Token CLOSEPAREN31=null;
		ParserRuleReturnScope argument30 =null;

		Object OPENPAREN29_tree=null;
		Object CLOSEPAREN31_tree=null;

		try {
			// Syn.g:41:9: ( OPENPAREN ! argument CLOSEPAREN !)
			// Syn.g:42:5: OPENPAREN ! argument CLOSEPAREN !
			{
			root_0 = (Object)adaptor.nil();


			OPENPAREN29=(Token)match(input,OPENPAREN,FOLLOW_OPENPAREN_in_content199); if (state.failed) return retval;
			pushFollow(FOLLOW_argument_in_content202);
			argument30=argument();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, argument30.getTree());

			CLOSEPAREN31=(Token)match(input,CLOSEPAREN,FOLLOW_CLOSEPAREN_in_content204); if (state.failed) return retval;
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "content"


	public static class argument_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "argument"
	// Syn.g:45:1: argument : ( string | expression | boolexp );
	public final Syn.argument_return argument() throws RecognitionException {
		Syn.argument_return retval = new Syn.argument_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope string32 =null;
		ParserRuleReturnScope expression33 =null;
		ParserRuleReturnScope boolexp34 =null;


		try {
			// Syn.g:45:10: ( string | expression | boolexp )
			int alt3=3;
			switch ( input.LA(1) ) {
			case STRING:
				{
				alt3=1;
				}
				break;
			case VARIABLE:
				{
				int LA3_2 = input.LA(2);
				if ( (synpred10_Syn()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case OPENPAREN:
				{
				int LA3_3 = input.LA(2);
				if ( (synpred10_Syn()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case INTNUM:
				{
				int LA3_4 = input.LA(2);
				if ( (synpred10_Syn()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case REALNUM:
				{
				int LA3_5 = input.LA(2);
				if ( (synpred10_Syn()) ) {
					alt3=2;
				}
				else if ( (true) ) {
					alt3=3;
				}

				}
				break;
			case FALSE:
			case NON:
			case TRUE:
				{
				alt3=3;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}
			switch (alt3) {
				case 1 :
					// Syn.g:46:5: string
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_string_in_argument220);
					string32=string();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, string32.getTree());

					}
					break;
				case 2 :
					// Syn.g:47:5: expression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_expression_in_argument226);
					expression33=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression33.getTree());

					}
					break;
				case 3 :
					// Syn.g:48:5: boolexp
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_boolexp_in_argument232);
					boolexp34=boolexp();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, boolexp34.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "argument"


	public static class boolexp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "boolexp"
	// Syn.g:51:1: boolexp : boolterm ( ( AND ^| OR ^) boolterm )* ;
	public final Syn.boolexp_return boolexp() throws RecognitionException {
		Syn.boolexp_return retval = new Syn.boolexp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token AND36=null;
		Token OR37=null;
		ParserRuleReturnScope boolterm35 =null;
		ParserRuleReturnScope boolterm38 =null;

		Object AND36_tree=null;
		Object OR37_tree=null;

		try {
			// Syn.g:51:9: ( boolterm ( ( AND ^| OR ^) boolterm )* )
			// Syn.g:52:5: boolterm ( ( AND ^| OR ^) boolterm )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_boolterm_in_boolexp247);
			boolterm35=boolterm();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, boolterm35.getTree());

			// Syn.g:52:14: ( ( AND ^| OR ^) boolterm )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==AND||LA5_0==OR) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// Syn.g:52:16: ( AND ^| OR ^) boolterm
					{
					// Syn.g:52:16: ( AND ^| OR ^)
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==AND) ) {
						alt4=1;
					}
					else if ( (LA4_0==OR) ) {
						alt4=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 4, 0, input);
						throw nvae;
					}

					switch (alt4) {
						case 1 :
							// Syn.g:52:18: AND ^
							{
							AND36=(Token)match(input,AND,FOLLOW_AND_in_boolexp253); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							AND36_tree = (Object)adaptor.create(AND36);
							root_0 = (Object)adaptor.becomeRoot(AND36_tree, root_0);
							}

							}
							break;
						case 2 :
							// Syn.g:52:25: OR ^
							{
							OR37=(Token)match(input,OR,FOLLOW_OR_in_boolexp258); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							OR37_tree = (Object)adaptor.create(OR37);
							root_0 = (Object)adaptor.becomeRoot(OR37_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_boolterm_in_boolexp263);
					boolterm38=boolterm();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, boolterm38.getTree());

					}
					break;

				default :
					break loop5;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "boolexp"


	public static class boolterm_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "boolterm"
	// Syn.g:55:1: boolterm : ( NON ^ bool | bool );
	public final Syn.boolterm_return boolterm() throws RecognitionException {
		Syn.boolterm_return retval = new Syn.boolterm_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NON39=null;
		ParserRuleReturnScope bool40 =null;
		ParserRuleReturnScope bool41 =null;

		Object NON39_tree=null;

		try {
			// Syn.g:55:10: ( NON ^ bool | bool )
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==NON) ) {
				alt6=1;
			}
			else if ( (LA6_0==FALSE||LA6_0==INTNUM||LA6_0==OPENPAREN||LA6_0==REALNUM||(LA6_0 >= TRUE && LA6_0 <= VARIABLE)) ) {
				alt6=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// Syn.g:56:5: NON ^ bool
					{
					root_0 = (Object)adaptor.nil();


					NON39=(Token)match(input,NON,FOLLOW_NON_in_boolterm280); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NON39_tree = (Object)adaptor.create(NON39);
					root_0 = (Object)adaptor.becomeRoot(NON39_tree, root_0);
					}

					pushFollow(FOLLOW_bool_in_boolterm283);
					bool40=bool();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, bool40.getTree());

					}
					break;
				case 2 :
					// Syn.g:57:5: bool
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_bool_in_boolterm289);
					bool41=bool();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, bool41.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "boolterm"


	public static class bool_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "bool"
	// Syn.g:60:1: bool : ( TRUE | FALSE | expression compare ^ expression | OPENPAREN ! boolexp CLOSEPAREN !);
	public final Syn.bool_return bool() throws RecognitionException {
		Syn.bool_return retval = new Syn.bool_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token TRUE42=null;
		Token FALSE43=null;
		Token OPENPAREN47=null;
		Token CLOSEPAREN49=null;
		ParserRuleReturnScope expression44 =null;
		ParserRuleReturnScope compare45 =null;
		ParserRuleReturnScope expression46 =null;
		ParserRuleReturnScope boolexp48 =null;

		Object TRUE42_tree=null;
		Object FALSE43_tree=null;
		Object OPENPAREN47_tree=null;
		Object CLOSEPAREN49_tree=null;

		try {
			// Syn.g:60:6: ( TRUE | FALSE | expression compare ^ expression | OPENPAREN ! boolexp CLOSEPAREN !)
			int alt7=4;
			switch ( input.LA(1) ) {
			case TRUE:
				{
				alt7=1;
				}
				break;
			case FALSE:
				{
				alt7=2;
				}
				break;
			case INTNUM:
			case REALNUM:
			case VARIABLE:
				{
				alt7=3;
				}
				break;
			case OPENPAREN:
				{
				int LA7_4 = input.LA(2);
				if ( (synpred16_Syn()) ) {
					alt7=3;
				}
				else if ( (true) ) {
					alt7=4;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}
			switch (alt7) {
				case 1 :
					// Syn.g:61:5: TRUE
					{
					root_0 = (Object)adaptor.nil();


					TRUE42=(Token)match(input,TRUE,FOLLOW_TRUE_in_bool304); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					TRUE42_tree = (Object)adaptor.create(TRUE42);
					adaptor.addChild(root_0, TRUE42_tree);
					}

					}
					break;
				case 2 :
					// Syn.g:62:5: FALSE
					{
					root_0 = (Object)adaptor.nil();


					FALSE43=(Token)match(input,FALSE,FOLLOW_FALSE_in_bool310); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					FALSE43_tree = (Object)adaptor.create(FALSE43);
					adaptor.addChild(root_0, FALSE43_tree);
					}

					}
					break;
				case 3 :
					// Syn.g:63:5: expression compare ^ expression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_expression_in_bool316);
					expression44=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression44.getTree());

					pushFollow(FOLLOW_compare_in_bool318);
					compare45=compare();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) root_0 = (Object)adaptor.becomeRoot(compare45.getTree(), root_0);
					pushFollow(FOLLOW_expression_in_bool321);
					expression46=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression46.getTree());

					}
					break;
				case 4 :
					// Syn.g:64:5: OPENPAREN ! boolexp CLOSEPAREN !
					{
					root_0 = (Object)adaptor.nil();


					OPENPAREN47=(Token)match(input,OPENPAREN,FOLLOW_OPENPAREN_in_bool327); if (state.failed) return retval;
					pushFollow(FOLLOW_boolexp_in_bool330);
					boolexp48=boolexp();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, boolexp48.getTree());

					CLOSEPAREN49=(Token)match(input,CLOSEPAREN,FOLLOW_CLOSEPAREN_in_bool332); if (state.failed) return retval;
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "bool"


	public static class compare_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "compare"
	// Syn.g:67:1: compare : ( EQUAL | LESSEQUAL | GREATER | GREATEREQUAL | LESS );
	public final Syn.compare_return compare() throws RecognitionException {
		Syn.compare_return retval = new Syn.compare_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set50=null;

		Object set50_tree=null;

		try {
			// Syn.g:67:9: ( EQUAL | LESSEQUAL | GREATER | GREATEREQUAL | LESS )
			// Syn.g:
			{
			root_0 = (Object)adaptor.nil();


			set50=input.LT(1);
			if ( input.LA(1)==EQUAL||(input.LA(1) >= GREATER && input.LA(1) <= GREATEREQUAL)||(input.LA(1) >= LESS && input.LA(1) <= LESSEQUAL) ) {
				input.consume();
				if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set50));
				state.errorRecovery=false;
				state.failed=false;
			}
			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "compare"


	public static class expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// Syn.g:75:1: expression : term ( ( PLUS ^| MINUS ^) term )* ;
	public final Syn.expression_return expression() throws RecognitionException {
		Syn.expression_return retval = new Syn.expression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PLUS52=null;
		Token MINUS53=null;
		ParserRuleReturnScope term51 =null;
		ParserRuleReturnScope term54 =null;

		Object PLUS52_tree=null;
		Object MINUS53_tree=null;

		try {
			// Syn.g:75:12: ( term ( ( PLUS ^| MINUS ^) term )* )
			// Syn.g:76:5: term ( ( PLUS ^| MINUS ^) term )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_term_in_expression387);
			term51=term();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, term51.getTree());

			// Syn.g:76:10: ( ( PLUS ^| MINUS ^) term )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==MINUS||LA9_0==PLUS) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// Syn.g:76:12: ( PLUS ^| MINUS ^) term
					{
					// Syn.g:76:12: ( PLUS ^| MINUS ^)
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0==PLUS) ) {
						alt8=1;
					}
					else if ( (LA8_0==MINUS) ) {
						alt8=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 8, 0, input);
						throw nvae;
					}

					switch (alt8) {
						case 1 :
							// Syn.g:76:14: PLUS ^
							{
							PLUS52=(Token)match(input,PLUS,FOLLOW_PLUS_in_expression393); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							PLUS52_tree = (Object)adaptor.create(PLUS52);
							root_0 = (Object)adaptor.becomeRoot(PLUS52_tree, root_0);
							}

							}
							break;
						case 2 :
							// Syn.g:76:22: MINUS ^
							{
							MINUS53=(Token)match(input,MINUS,FOLLOW_MINUS_in_expression398); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							MINUS53_tree = (Object)adaptor.create(MINUS53);
							root_0 = (Object)adaptor.becomeRoot(MINUS53_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_term_in_expression403);
					term54=term();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, term54.getTree());

					}
					break;

				default :
					break loop9;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class term_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "term"
	// Syn.g:79:1: term : factor ( ( MULTIPLY ^| DIV ^) factor )* ;
	public final Syn.term_return term() throws RecognitionException {
		Syn.term_return retval = new Syn.term_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MULTIPLY56=null;
		Token DIV57=null;
		ParserRuleReturnScope factor55 =null;
		ParserRuleReturnScope factor58 =null;

		Object MULTIPLY56_tree=null;
		Object DIV57_tree=null;

		try {
			// Syn.g:79:6: ( factor ( ( MULTIPLY ^| DIV ^) factor )* )
			// Syn.g:80:5: factor ( ( MULTIPLY ^| DIV ^) factor )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_factor_in_term420);
			factor55=factor();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, factor55.getTree());

			// Syn.g:80:12: ( ( MULTIPLY ^| DIV ^) factor )*
			loop11:
			while (true) {
				int alt11=2;
				int LA11_0 = input.LA(1);
				if ( (LA11_0==DIV||LA11_0==MULTIPLY) ) {
					alt11=1;
				}

				switch (alt11) {
				case 1 :
					// Syn.g:80:14: ( MULTIPLY ^| DIV ^) factor
					{
					// Syn.g:80:14: ( MULTIPLY ^| DIV ^)
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0==MULTIPLY) ) {
						alt10=1;
					}
					else if ( (LA10_0==DIV) ) {
						alt10=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						NoViableAltException nvae =
							new NoViableAltException("", 10, 0, input);
						throw nvae;
					}

					switch (alt10) {
						case 1 :
							// Syn.g:80:15: MULTIPLY ^
							{
							MULTIPLY56=(Token)match(input,MULTIPLY,FOLLOW_MULTIPLY_in_term425); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							MULTIPLY56_tree = (Object)adaptor.create(MULTIPLY56);
							root_0 = (Object)adaptor.becomeRoot(MULTIPLY56_tree, root_0);
							}

							}
							break;
						case 2 :
							// Syn.g:80:27: DIV ^
							{
							DIV57=(Token)match(input,DIV,FOLLOW_DIV_in_term430); if (state.failed) return retval;
							if ( state.backtracking==0 ) {
							DIV57_tree = (Object)adaptor.create(DIV57);
							root_0 = (Object)adaptor.becomeRoot(DIV57_tree, root_0);
							}

							}
							break;

					}

					pushFollow(FOLLOW_factor_in_term435);
					factor58=factor();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, factor58.getTree());

					}
					break;

				default :
					break loop11;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "term"


	public static class factor_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "factor"
	// Syn.g:83:1: factor : ( VARIABLE | OPENPAREN ! expression CLOSEPAREN !| INTNUM | REALNUM );
	public final Syn.factor_return factor() throws RecognitionException {
		Syn.factor_return retval = new Syn.factor_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token VARIABLE59=null;
		Token OPENPAREN60=null;
		Token CLOSEPAREN62=null;
		Token INTNUM63=null;
		Token REALNUM64=null;
		ParserRuleReturnScope expression61 =null;

		Object VARIABLE59_tree=null;
		Object OPENPAREN60_tree=null;
		Object CLOSEPAREN62_tree=null;
		Object INTNUM63_tree=null;
		Object REALNUM64_tree=null;

		try {
			// Syn.g:83:8: ( VARIABLE | OPENPAREN ! expression CLOSEPAREN !| INTNUM | REALNUM )
			int alt12=4;
			switch ( input.LA(1) ) {
			case VARIABLE:
				{
				alt12=1;
				}
				break;
			case OPENPAREN:
				{
				alt12=2;
				}
				break;
			case INTNUM:
				{
				alt12=3;
				}
				break;
			case REALNUM:
				{
				alt12=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}
			switch (alt12) {
				case 1 :
					// Syn.g:84:5: VARIABLE
					{
					root_0 = (Object)adaptor.nil();


					VARIABLE59=(Token)match(input,VARIABLE,FOLLOW_VARIABLE_in_factor452); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					VARIABLE59_tree = (Object)adaptor.create(VARIABLE59);
					adaptor.addChild(root_0, VARIABLE59_tree);
					}

					}
					break;
				case 2 :
					// Syn.g:85:5: OPENPAREN ! expression CLOSEPAREN !
					{
					root_0 = (Object)adaptor.nil();


					OPENPAREN60=(Token)match(input,OPENPAREN,FOLLOW_OPENPAREN_in_factor458); if (state.failed) return retval;
					pushFollow(FOLLOW_expression_in_factor461);
					expression61=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression61.getTree());

					CLOSEPAREN62=(Token)match(input,CLOSEPAREN,FOLLOW_CLOSEPAREN_in_factor463); if (state.failed) return retval;
					}
					break;
				case 3 :
					// Syn.g:86:5: INTNUM
					{
					root_0 = (Object)adaptor.nil();


					INTNUM63=(Token)match(input,INTNUM,FOLLOW_INTNUM_in_factor470); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					INTNUM63_tree = (Object)adaptor.create(INTNUM63);
					adaptor.addChild(root_0, INTNUM63_tree);
					}

					}
					break;
				case 4 :
					// Syn.g:87:5: REALNUM
					{
					root_0 = (Object)adaptor.nil();


					REALNUM64=(Token)match(input,REALNUM,FOLLOW_REALNUM_in_factor476); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					REALNUM64_tree = (Object)adaptor.create(REALNUM64);
					adaptor.addChild(root_0, REALNUM64_tree);
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "factor"


	protected static class string_scope {
		String tmp;
	}
	protected Stack<string_scope> string_stack = new Stack<string_scope>();

	public static class string_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "string"
	// Syn.g:90:1: string : s= STRING -> STRING[$string::tmp] ;
	public final Syn.string_return string() throws RecognitionException {
		string_stack.push(new string_scope());
		Syn.string_return retval = new Syn.string_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token s=null;

		Object s_tree=null;
		RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");

		try {
			// Syn.g:92:5: (s= STRING -> STRING[$string::tmp] )
			// Syn.g:93:5: s= STRING
			{
			s=(Token)match(input,STRING,FOLLOW_STRING_in_string505); if (state.failed) return retval; 
			if ( state.backtracking==0 ) stream_STRING.add(s);

			if ( state.backtracking==0 ) { string_stack.peek().tmp = cleanString((s!=null?s.getText():null)); }
			// AST REWRITE
			// elements: STRING
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			if ( state.backtracking==0 ) {
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 93:54: -> STRING[$string::tmp]
			{
				adaptor.addChild(root_0, (Object)adaptor.create(STRING, string_stack.peek().tmp));
			}


			retval.tree = root_0;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
			string_stack.pop();
		}
		return retval;
	}
	// $ANTLR end "string"

	// $ANTLR start synpred10_Syn
	public final void synpred10_Syn_fragment() throws RecognitionException {
		// Syn.g:47:5: ( expression )
		// Syn.g:47:5: expression
		{
		pushFollow(FOLLOW_expression_in_synpred10_Syn226);
		expression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred10_Syn

	// $ANTLR start synpred16_Syn
	public final void synpred16_Syn_fragment() throws RecognitionException {
		// Syn.g:63:5: ( expression compare expression )
		// Syn.g:63:5: expression compare expression
		{
		pushFollow(FOLLOW_expression_in_synpred16_Syn316);
		expression();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_compare_in_synpred16_Syn318);
		compare();
		state._fsp--;
		if (state.failed) return;

		pushFollow(FOLLOW_expression_in_synpred16_Syn321);
		expression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred16_Syn

	// Delegated rules

	public final boolean synpred10_Syn() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred10_Syn_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred16_Syn() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred16_Syn_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}



	public static final BitSet FOLLOW_statements_in_program56 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_in_statements71 = new BitSet(new long[]{0x0000000008000002L});
	public static final BitSet FOLLOW_SEMICOLON_in_statements75 = new BitSet(new long[]{0x0000000F12408000L});
	public static final BitSet FOLLOW_statement_in_statements78 = new BitSet(new long[]{0x0000000008000002L});
	public static final BitSet FOLLOW_WRITE_in_statement96 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_content_in_statement99 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WRITELN_in_statement105 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_READ_in_statement112 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_OPENPAREN_in_statement115 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_VARIABLE_in_statement118 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLOSEPAREN_in_statement120 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VARIABLE_in_statement127 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ASSIGN_in_statement129 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_expression_in_statement132 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_statement138 = new BitSet(new long[]{0x0000000184611000L});
	public static final BitSet FOLLOW_boolexp_in_statement141 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_THEN_in_statement143 = new BitSet(new long[]{0x0000000F12408000L});
	public static final BitSet FOLLOW_statement_in_statement146 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_ELSE_in_statement148 = new BitSet(new long[]{0x0000000F12408000L});
	public static final BitSet FOLLOW_statement_in_statement151 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHILE_in_statement157 = new BitSet(new long[]{0x0000000184611000L});
	public static final BitSet FOLLOW_boolexp_in_statement160 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_DO_in_statement162 = new BitSet(new long[]{0x0000000F12408000L});
	public static final BitSet FOLLOW_statement_in_statement165 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OPENPAREN_in_statement171 = new BitSet(new long[]{0x0000000F12408000L});
	public static final BitSet FOLLOW_statements_in_statement174 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLOSEPAREN_in_statement176 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SKIP_in_statement183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OPENPAREN_in_content199 = new BitSet(new long[]{0x00000001A4611000L});
	public static final BitSet FOLLOW_argument_in_content202 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLOSEPAREN_in_content204 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_argument220 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_argument226 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_boolexp_in_argument232 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_boolterm_in_boolexp247 = new BitSet(new long[]{0x0000000000800012L});
	public static final BitSet FOLLOW_AND_in_boolexp253 = new BitSet(new long[]{0x0000000184611000L});
	public static final BitSet FOLLOW_OR_in_boolexp258 = new BitSet(new long[]{0x0000000184611000L});
	public static final BitSet FOLLOW_boolterm_in_boolexp263 = new BitSet(new long[]{0x0000000000800012L});
	public static final BitSet FOLLOW_NON_in_boolterm280 = new BitSet(new long[]{0x0000000184411000L});
	public static final BitSet FOLLOW_bool_in_boolterm283 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_bool_in_boolterm289 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_bool304 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_bool310 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_bool316 = new BitSet(new long[]{0x0000000000066800L});
	public static final BitSet FOLLOW_compare_in_bool318 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_expression_in_bool321 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OPENPAREN_in_bool327 = new BitSet(new long[]{0x0000000184611000L});
	public static final BitSet FOLLOW_boolexp_in_bool330 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLOSEPAREN_in_bool332 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_expression387 = new BitSet(new long[]{0x0000000001080002L});
	public static final BitSet FOLLOW_PLUS_in_expression393 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_MINUS_in_expression398 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_term_in_expression403 = new BitSet(new long[]{0x0000000001080002L});
	public static final BitSet FOLLOW_factor_in_term420 = new BitSet(new long[]{0x0000000000100102L});
	public static final BitSet FOLLOW_MULTIPLY_in_term425 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_DIV_in_term430 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_factor_in_term435 = new BitSet(new long[]{0x0000000000100102L});
	public static final BitSet FOLLOW_VARIABLE_in_factor452 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OPENPAREN_in_factor458 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_expression_in_factor461 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLOSEPAREN_in_factor463 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INTNUM_in_factor470 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_REALNUM_in_factor476 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_string505 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_synpred10_Syn226 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_synpred16_Syn316 = new BitSet(new long[]{0x0000000000066800L});
	public static final BitSet FOLLOW_compare_in_synpred16_Syn318 = new BitSet(new long[]{0x0000000104410000L});
	public static final BitSet FOLLOW_expression_in_synpred16_Syn321 = new BitSet(new long[]{0x0000000000000002L});
}
