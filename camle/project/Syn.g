// COMS22201: Syntax analyser

parser grammar Syn;

options {
  backtrack = true;
  tokenVocab = Lex;
  output = AST;
}

@members
{
	private String cleanString(String s){
		String tmp;
		tmp = s.replaceAll("^'", "");
		s = tmp.replaceAll("'$", "");
		tmp = s.replaceAll("''", "'");
		return tmp;
	}
}

program :
    statements
  ;

statements :
    statement ( SEMICOLON^ statement )*
  ;

statement :
    WRITE^ content
  | WRITELN^
  | READ^ OPENPAREN! VARIABLE CLOSEPAREN!
  | VARIABLE ASSIGN^ expression
  | IF^ boolexp THEN! statement ELSE! statement
  | WHILE^ boolexp DO! statement
  | OPENPAREN! statements CLOSEPAREN!
  | SKIP^
  ;

content :
    OPENPAREN! argument CLOSEPAREN!
  ;

argument :
    string
  | expression
  | boolexp
  ;

boolexp :
    boolterm ( ( AND^ | OR^ ) boolterm)*
  ;

boolterm :
    NON^ bool
  | bool
  ;

bool :
    TRUE
  | FALSE
  | expression compare^ expression
  | OPENPAREN! boolexp CLOSEPAREN!
  ;

compare :
    EQUAL
  | LESSEQUAL
  | GREATER
  | GREATEREQUAL
  | LESS
  ;

expression :
    term ( ( PLUS^ | MINUS^ ) term)*
  ;

term :
    factor ( (MULTIPLY^ | DIV^ ) factor)*
  ;

factor :
    VARIABLE
  | OPENPAREN! expression CLOSEPAREN!
  | INTNUM
  | REALNUM
  ;

string
    scope { String tmp; }
    :
    s=STRING { $string::tmp = cleanString($s.text); }-> STRING[$string::tmp]
  ;


