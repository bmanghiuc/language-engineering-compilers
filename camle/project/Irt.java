// COMS22201: IR tree construction
//Manghiuc Bogdan
import java.util.*;
import java.io.*;
import java.lang.reflect.Array;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class Irt
{
// The code below is generated automatically from the ".tokens" file of the 
// ANTLR syntax analysis, using the TokenConv program.
//
// CAMLE TOKENS BEGIN
  public static final String[] tokenNames = new String[] {
"NONE", "NONE", "NONE", "NONE", "AND", "ASSIGN", "CLOSEPAREN", "COMMENT", "DIV", "DO", "ELSE", "EQUAL", "FALSE", "GREATER", "GREATEREQUAL", "IF", "INTNUM", "LESS", "LESSEQUAL", "MINUS", "MULTIPLY", "NON", "OPENPAREN", "OR", "PLUS", "READ", "REALNUM", "SEMICOLON", "SKIP", "STRING", "THEN", "TRUE", "VARIABLE", "WHILE", "WRITE", "WRITELN", "WS"};
  public static final int AND=4;
  public static final int ASSIGN=5;
  public static final int CLOSEPAREN=6;
  public static final int COMMENT=7;
  public static final int DIV=8;
  public static final int DO=9;
  public static final int ELSE=10;
  public static final int EQUAL=11;
  public static final int FALSE=12;
  public static final int GREATER=13;
  public static final int GREATEREQUAL=14;
  public static final int IF=15;
  public static final int INTNUM=16;
  public static final int LESS=17;
  public static final int LESSEQUAL=18;
  public static final int MINUS=19;
  public static final int MULTIPLY=20;
  public static final int NON=21;
  public static final int OPENPAREN=22;
  public static final int OR=23;
  public static final int PLUS=24;
  public static final int READ=25;
  public static final int REALNUM=26;
  public static final int SEMICOLON=27;
  public static final int SKIP=28;
  public static final int STRING=29;
  public static final int THEN=30;
  public static final int TRUE=31;
  public static final int VARIABLE=32;
  public static final int WHILE=33;
  public static final int WRITE=34;
  public static final int WRITELN=35;
  public static final int WS=36;
// CAMLE TOKENS END
  public static int labelNumber = 1;
  public static IRTree convert(CommonTree ast)
  {
    IRTree irt = new IRTree();
    program(ast, irt);
    return irt;
  }

  public static void program(CommonTree ast, IRTree irt)
  {
    statements(ast, irt);
  }

  public static void statements(CommonTree ast, IRTree irt)
  {
    int i;
    Token t = ast.getToken();
    int tt = t.getType();
    if (tt == SEMICOLON) {
      IRTree irt1 = new IRTree();
      IRTree irt2 = new IRTree();
      CommonTree ast1 = (CommonTree)ast.getChild(0);
      CommonTree ast2 = (CommonTree)ast.getChild(1);
      statements(ast1, irt1);
      statements(ast2, irt2);
      irt.setOp("SEQ");
      irt.addSub(irt1);
      irt.addSub(irt2);
    }
    else {
      statement(ast, irt);
    }
  }

  public static void statement(CommonTree ast, IRTree irt)
  {
    CommonTree ast1, ast2, ast3;
    IRTree irt1 = new IRTree(), irt2 = new IRTree(), irt3 = new IRTree();
    Token t = ast.getToken();
    int tt = t.getType();
    if (tt == WRITE) {
      ast1 = (CommonTree)ast.getChild(0);
      String type = arg(ast1, irt1);
      if (type.equals("int")) {
        irt.setOp("WR");
        irt.addSub(irt1);
      }
      else if(type.equals("real")){
	irt.setOp("WRR");
	irt.addSub(irt1);
      }
      else if(type.equals("string")){
        irt.setOp("WRS");
        irt.addSub(irt1);
      }
      else if(type.equals("bool")){
        //Make CJUMP IRTree
        irt.setOp("SEQ");
        IRTree conditionalJump = new IRTree("CJUMP");
        conditionalJump.addSub(new IRTree("BOOLEXP", irt1));
        IRTree label1 = new IRTree("L" + labelNumber);
        labelNumber ++;
        IRTree label2 = new IRTree("L"+ labelNumber);
        labelNumber ++;
        conditionalJump.addSub(label1);
        conditionalJump.addSub(label2);
        irt.addSub(conditionalJump);

        //Get IRTree for true
        IRTree instruction1 = new IRTree();
        int a = Memory.allocateString("true");
        String st = String.valueOf(a);
        instruction1.setOp("MEM");
        instruction1.addSub(new IRTree("CONST", new IRTree(st)));        
        
        //Get IRTree for false
        a = Memory.allocateString("false");
        st = String.valueOf(a);
        IRTree instruction2 = new IRTree();
        instruction2.setOp("MEM");
        instruction2.addSub(new IRTree("CONST", new IRTree(st))); 

        //True label
        IRTree seqLabel1 = new IRTree("SEQ");
        irt.addSub(seqLabel1);
        seqLabel1.addSub(new IRTree("LABEL", label1));
        IRTree seqInstruction1 = new IRTree("SEQ");
        seqLabel1.addSub(seqInstruction1);
        seqInstruction1.addSub(new IRTree("WRS", instruction1));

        //Combine the true label and the false label
        IRTree seqJumpBetweenInstructions = new IRTree("SEQ");
        seqInstruction1.addSub(seqJumpBetweenInstructions);
        seqJumpBetweenInstructions.addSub(new IRTree("JUMP", new IRTree("L" + labelNumber)));
        labelNumber++;
        
        //False label
        IRTree seqLabel2 = new IRTree("SEQ");
        seqJumpBetweenInstructions.addSub(seqLabel2);
        seqLabel2.addSub(new IRTree("LABEL", label2));
        IRTree seqInstruction2 = new IRTree("SEQ");
        seqLabel2.addSub(seqInstruction2);
        seqInstruction2.addSub(new IRTree("WRS", instruction2));
       
        //Combine the false label to the rest of the program
        seqInstruction2.addSub(new IRTree("LABEL", new IRTree("L" + (labelNumber-1))));
      }
    }
    else if (tt == WRITELN) {
      String a = String.valueOf(Memory.allocateString("\n"));
      irt.setOp("WRS");
      irt.addSub(new IRTree("MEM", new IRTree("CONST", new IRTree(a))));
    }
    else if (tt == ASSIGN) {
      ast1 = (CommonTree)ast.getChild(0);
      ast2 = (CommonTree)ast.getChild(1);
      irt.setOp("MOVE");
      String e = expression(ast2, irt2);
      if(e.equals("real"))
      	term(ast1, irt1, "real");
      else
        term(ast1,irt1, "int");
      irt.addSub(irt1);
      irt.addSub(irt2);
    }
    else if (tt == READ){
      ast1 = (CommonTree)ast.getChild(0);
      String type = arg(ast1, irt1);
      if(type.equals("int")) {
        irt.setOp("RD");
        irt.addSub(irt1);
      }
      else if(type.equals("real")) {
        irt.setOp("RDR");
        irt.addSub(irt1);
      }
    }
    else if (tt == IF){
     //Get IRTrees from ast statements
      ast1 =  (CommonTree) ast.getChild(1);
      ast2 =  (CommonTree) ast.getChild(2);
      statements(ast1, irt1);
      statements(ast2, irt2);
    

     //Make the CJUMP IRTree
      irt.setOp("SEQ");
      ast3 = (CommonTree) ast.getChild(0);
      IRTree conditionalJump = new IRTree("CJUMP");
      IRTree boolexp = new IRTree();
      translate(ast3, boolexp);
      conditionalJump.addSub(new IRTree("BOOLEXP", boolexp));
      IRTree label1 = new IRTree("L" + labelNumber);
      labelNumber ++;
      IRTree label2 = new IRTree("L"+ labelNumber);
      labelNumber ++;
      conditionalJump.addSub(label1);
      conditionalJump.addSub(label2);
      irt.addSub(conditionalJump);

      //True label
      IRTree seqLabel1 = new IRTree("SEQ");
      irt.addSub(seqLabel1);
      seqLabel1.addSub(new IRTree("LABEL", label1));
      IRTree seqInstruction1 = new IRTree("SEQ");
      seqLabel1.addSub(seqInstruction1);
      seqInstruction1.addSub(irt1);

      //Combine the true label and the false label
      IRTree seqJumpBetweenInstructions = new IRTree("SEQ");
      seqInstruction1.addSub(seqJumpBetweenInstructions);
      seqJumpBetweenInstructions.addSub(new IRTree("JUMP", new IRTree("L" + labelNumber)));
      labelNumber++;

      //False label
      IRTree seqLabel2 = new IRTree("SEQ");
      seqJumpBetweenInstructions.addSub(seqLabel2);
      seqLabel2.addSub(new IRTree("LABEL", label2));
      IRTree seqInstruction2 = new IRTree("SEQ");
      seqLabel2.addSub(seqInstruction2);
      seqInstruction2.addSub(irt2);

      //Combine the false label with the rest of the program
      seqInstruction2.addSub(new IRTree("LABEL", new IRTree("L" + (labelNumber-1))));
    }
    else if(tt == WHILE){
      //Get IRTree from ast statement
      ast1 = (CommonTree) ast.getChild(1);
      statements(ast1, irt1);

      //The looping label
      irt.setOp("SEQ");
      IRTree label1 = new IRTree("L" +  labelNumber);
      labelNumber++;
      irt.addSub(new IRTree("LABEL", label1));

      //Make the CJUMP IRTree      
      IRTree seqConditional = new IRTree("SEQ");
      irt.addSub(seqConditional);  
      ast2 = (CommonTree) ast.getChild(0);
      IRTree conditionalJump = new IRTree("CJUMP");
      IRTree boolexp = new IRTree();
      translate(ast2, boolexp);
      conditionalJump.addSub(new IRTree("BOOLEXP", boolexp));
      IRTree label2 = new IRTree("L" + labelNumber);
      labelNumber++;
      IRTree label3 = new IRTree("L" + labelNumber);
      labelNumber++;
      conditionalJump.addSub(label2);
      conditionalJump.addSub(label3);
      seqConditional.addSub(conditionalJump);
         
      //Looping state
      IRTree seqLabel2 = new IRTree("SEQ");
      seqConditional.addSub(seqLabel2);
      seqLabel2.addSub(new IRTree("LABEL",label2));
      IRTree seqInstruction = new IRTree("SEQ");
      seqLabel2.addSub(seqInstruction);
      seqInstruction.addSub(irt1);
      
      //End of the loop
      IRTree seqEndLoop = new IRTree("SEQ");
      seqInstruction.addSub(seqEndLoop);
      seqEndLoop.addSub(new IRTree("JUMP", label1));
      seqEndLoop.addSub(new IRTree("LABEL",label3));  
    }
    else if(tt == SKIP){
      irt.setOp("NOP");
    }
    else if(tt == LESS){
      irt.setOp("LESS");
    }
    else if(tt == LESSEQUAL){
      irt.setOp("LESSEQUAL");
    }
    else if(tt == GREATER){
      irt.setOp("GREATER");
    }  
    else if(tt == GREATEREQUAL){
      irt.setOp("GREATEREQUAL");
    }
    else if(tt == EQUAL){
      irt.setOp("EQUAL");
    }        
    else {
      error(tt);
    }
  }
  public static String arg(CommonTree ast, IRTree irt)
  {
    Token t = ast.getToken();
    int tt = t.getType();
    if (tt == STRING) {
      String tx = t.getText();
      int a = Memory.allocateString(tx); 
      String st = String.valueOf(a);
      irt.setOp("MEM");
      irt.addSub(new IRTree("CONST", new IRTree(st)));
      return "string";
    }
    else {
      String e = expression(ast, irt);
      return e;
    }
  }

  public static void translate(CommonTree ast, IRTree irt)
  {
    CommonTree ast1, ast2;
    IRTree irt1 = new IRTree(), irt2 = new IRTree();
    Token t = ast.getToken();
    int tt = t.getType();
    if(tt == TRUE) {
       irt.setOp("true");
    } 
    else if(tt == FALSE) {
       irt.setOp("false");
    }
    else if(tt == LESS || tt == LESSEQUAL || tt == GREATER || tt == GREATEREQUAL || tt == EQUAL) {
      if(tt == LESS)
        irt.setOp("LESS");
      else if(tt == LESSEQUAL)
        irt.setOp("LESSEQUAL");
      else if(tt == GREATER)
        irt.setOp("GREATER");
      else if(tt == GREATEREQUAL)
        irt.setOp("GREATEREQUAL");
      else if(tt == EQUAL)
        irt.setOp("EQUAL");
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      expression(ast1, irt1);
      expression(ast2, irt2);
      irt.addSub(irt1);
      irt.addSub(irt2);
    }
    else if(tt == AND) {
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      translate(ast1, irt1);
      translate(ast2, irt2);
      irt.setOp("AND");
      irt.addSub(irt1);
      irt.addSub(irt2);
    }
    else if(tt == OR) {
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      translate(ast1, irt1);
      translate(ast2, irt2);
      irt.setOp("OR");
      irt.addSub(irt1);
      irt.addSub(irt2);
    }
    else if(tt == NON) {
      ast1 = (CommonTree) ast.getChild(0);
      translate(ast1, irt1);
      irt.setOp("NON");
      irt.addSub(irt1);
    }
    else{
       error(tt);
    }
  }
  public static String expression(CommonTree ast, IRTree irt)
  {
    CommonTree ast1, ast2;
    String e1, e2;
    IRTree irt1 = new IRTree(), irt2 = new IRTree();
    Token t = ast.getToken();
    int tt = t.getType();
    if (tt == INTNUM || tt == REALNUM) {
      constant(ast, irt1);
      irt.setOp("CONST");
      irt.addSub(irt1);
        if(tt == INTNUM)
          return "int";
        else
          return "real";
    }
    else if (tt == VARIABLE) {
      return term(ast, irt);
    }
    else if (tt == PLUS){
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      e1 = expression(ast1, irt1);
      e2 = expression(ast2, irt2);
      irt.setOp("BINOP");
      if(e1.equals(e2))
      	irt.addSub(new IRTree("+" + e1));
      else {
        System.out.println("ERROR: Multiple types in addition expression");
        System.exit(3);
        return "";
      } 
      irt.addSub(irt1);
      irt.addSub(irt2);
      return e1;
    }
    else if(tt == MINUS){
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      e1 = expression(ast1, irt1);
      e2 = expression(ast2, irt2);
      irt.setOp("BINOP");
      if(e1.equals(e2))
      	irt.addSub(new IRTree("-" + e1));
      else {
        System.out.println("ERROR: Multiple types in subtraction expression");
        System.exit(3);
        return "";
      } 
      irt.addSub(irt1);
      irt.addSub(irt2);
      return e1;
    }
    else if(tt == MULTIPLY){
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      e1 = expression(ast1, irt1);
      e2 = expression(ast2, irt2);
      irt.setOp("BINOP");
      if(e1.equals(e2))
      	irt.addSub(new IRTree("*" + e1));
      else {
        System.out.println("ERROR: Multiple types in multiplication expression");
        System.exit(3);
        return "";
      } 
      irt.addSub(irt1);
      irt.addSub(irt2);
      return e1;
    }
    else if(tt == DIV){
      ast1 = (CommonTree) ast.getChild(0);
      ast2 = (CommonTree) ast.getChild(1);
      e1 = expression(ast1, irt1);
      e2 = expression(ast2, irt2);
      irt.setOp("BINOP");
      if(e1.equals(e2))
      	irt.addSub(new IRTree("/" + e1));
      else {
        System.out.println("ERROR: Multiple types in division expression");
        System.exit(3);
        return "";
      } 
      irt.addSub(irt1);
      irt.addSub(irt2);
      return e1;
    }
    else if(tt == TRUE || tt == FALSE || tt == AND || tt == NON || tt == OR){
      translate(ast, irt);
      return "bool";
    }
    else{
      error(tt);
    }
    return "";
  }

  public static void constant(CommonTree ast, IRTree irt)
  {
    Token t = ast.getToken();
    int tt = t.getType();
    if (tt == INTNUM || tt == REALNUM) {
      String tx = t.getText();
      irt.setOp(tx);
    }
    else{
      error(tt);
    }
  }
  private static String term(CommonTree ast, IRTree irt, String type)
  {
    Token t = ast.getToken();
    int tt = t.getType();
    if(tt == VARIABLE){
      String tx = t.getText();
      if(tx.length() >=9 || tx.length()<1){
        System.out.println("ERROR : The length of the identifier " + tx + " is out of bounds");
        System.exit(2);
      }
      int a = Memory.allocateVariable(tx, type);
      String st = String.valueOf(a);
      irt.setOp("MEM");
      irt.addSub(new IRTree("CONST", new IRTree(st)));

    }
    return type;
  }
  private static String term(CommonTree ast, IRTree irt)
  {
    Token t = ast.getToken();
    int tt = t.getType();
    if(tt == VARIABLE){
      String tx = t.getText();
      int a = Memory.allocateVariable(tx);
      String st = String.valueOf(a);
      irt.setOp("MEM");
      irt.addSub(new IRTree("CONST", new IRTree(st)));
      if(Memory.memory.get(a).getContents() == 2)
        return "real";
      else
        return "int";
    }
    return "";
  }
  private static void error(int tt)
  {
    System.out.println("IRT error: "+tokenNames[tt]);
    System.exit(1);
  }
}
