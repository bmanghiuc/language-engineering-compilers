{COMS22201 test10: program to test OR(||) boolean operator.}
{Already tested: write statements, boolean expressions, real numbers and constants.}

write(true || true);
write(true || false);
write(false || false);
write(!true || ((true & false) || true));
write(false || 1.7 < 2.5)
