# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Contains all the files needed for the compiler to run on the language While

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: Simply download the files
* Configuration
* Dependencies: Antlr3, javac
* How to run tests: Simply modify in the makefile the number of the current test
* Deployment instructions

### Contribution guidelines ###

* Writing tests: All the tests have been written to verify all the features included in the While language as well as some further extensions of it
* Code review: The code has been created to fulfill the purpose of the first coursework in the unit Language Engineering at the University of Bristol, 2015 
* Other guidelines: The skeleton for the Lex.g, Syn.g, Irt.java and Cg.java as well as the other classes/files have already been provided at the beginning of the coursework.

### Who do I talk to? ###

* Repo owner or admin